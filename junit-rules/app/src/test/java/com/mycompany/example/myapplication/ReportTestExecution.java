package com.mycompany.example.myapplication;

import org.junit.rules.MethodRule;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.Statement;

/**
 * Created by TheDancerCodes on 07/03/2018.
 */


/**
 *                          This is a Custom Rule:
 *
 *   This is a JUnit rule where the developer provides the MethodRule implementation.
 *   It is written to provide custom pre and post processing in your unit tests.
 */

public class ReportTestExecution implements MethodRule {
    @Override
    public Statement apply(Statement base, FrameworkMethod method, Object target) {

        // Name of Test Class
        final String className = method.getMethod().getDeclaringClass().getSimpleName();

        // Test Method Executing
        final String methodName = method.getName();

        // return statement
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                System.out.println(String.format("Sending To Web Service: " +
                        "about to execute %s's method %s", className, methodName));
                this.evaluate();
                System.out.println(String.format("Sending To Web Service: " +
                        "about to execute %s's method %s", className, methodName));
            }
        };
    }
}
