package com.mycompany.example.myapplication;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {

    @Test
    public void testWithAsserts() {
        List<String> list = someListProducingMethod();
        assertTrue(list.contains("dog"));
        assertTrue(list.contains("fox"));
        assertTrue(list.size() > 3);
        assertTrue(list.size() < 12);
    }

    @Test
    public void testWithBigAssert() {
        List<String> list = someListProducingMethod();
        assertTrue(list.contains("dog") && list.contains("fox") && list.size() > 3 && list.size() < 12);
    }

    @Test
    public void testWithHamcrest() {
        List<String> list = someListProducingMethod();
        assertThat(list, (hasItems("dog", "fox")));
        assertThat(list, allOf(hasSize(greaterThan(3)), hasSize(lessThan(12))));
    }

    @Test
    public void testFailureWithAsserts() {
        List<String> list = someListProducingMethod();
        assertTrue(list.contains("dog"));
        assertTrue(list.contains("panda"));
        assertTrue(list.size() > 3);
        assertTrue(list.size() < 12);
    }

    @Test
    public void testFailureWithHamcrest() {
        List<String> list = someListProducingMethod();
        assertThat(list, (hasItems("dog", "panda")));
        assertThat(list, allOf(hasSize(greaterThan(3)), hasSize(lessThan(12))));
    }

    @Test
    public void testTypeSafety() {
        // Uncomment this to see the type safety checking between actual and expected values
//        assertThat("123", equalTo(123));
//        assertThat(123, equalTo("123"));
    }

    private List<String> someListProducingMethod() {
        String[] sentence = {"The", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"};
        return Arrays.asList(sentence);
    }
}