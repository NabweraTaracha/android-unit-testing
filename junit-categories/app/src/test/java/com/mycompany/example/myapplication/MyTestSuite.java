package com.mycompany.example.myapplication;

import com.mycompany.example.myapplication.categories.FastTests;
import com.mycompany.example.myapplication.categories.GoogleMapsIntegrationTests;
import com.mycompany.example.myapplication.categories.SlowTests;

import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by TheDancerCodes on 12/03/2018.
 */

@RunWith(Categories.class)
@Suite.SuiteClasses({SimpleMathTest.class, AnotherTest.class})

// Annotate the Test Suite with indications of which Categories of tests to include and exclude.
@Categories.IncludeCategory({FastTests.class})
@Categories.ExcludeCategory({GoogleMapsIntegrationTests.class})
public class MyTestSuite {
}
