package com.mycompany.example.myapplication;

import com.mycompany.example.myapplication.categories.ContactIntegrationTests;
import com.mycompany.example.myapplication.categories.FastTests;
import com.mycompany.example.myapplication.categories.InAppPurchaseTests;
import com.mycompany.example.myapplication.categories.SlowTests;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static org.junit.Assert.assertEquals;

//import com.mycompany.example.myapplication.categories.SlowTests;

/**
 * When you apply a Category to an entire test class, all test methods in that class
 * also get this Category
 */

@Category(FastTests.class)
public class SimpleMathTest {

    SimpleMath sm;

    @Before
    public void setup() {
        sm = new SimpleMath();
    }

    @Test
//    @Category(FastTests.class)
    public void testAdd() {
        assertEquals("SimpleMath addition not adding correctly", 9, sm.add(4, 5));
    }

    @Test
//    @Category(SlowTests.class)
    /**
     * Applying Category to an individual test method
     */
    @Category({InAppPurchaseTests.class, ContactIntegrationTests.class})
    public void testDiff() {
        assertEquals("SimpleMath diff not subtracting correctly", 5, sm.diff(12, 7));
    }

}
