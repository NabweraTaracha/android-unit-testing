package com.mycompany.example.myapplication;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleMathTest {

    // Create a variable
    private SimpleMath sm;

    @BeforeClass
    public static void testClassSetup() {
        System.out.println("Getting Test Class Ready");
    }

    @AfterClass
    public static void testClassCleanup() {
        System.out.println("Done with tests");
    }

    @Before
    public void setUp() {
        // Instance of the Domain object/ object under test
        sm = new SimpleMath();

        // To be able to see the execution of our before method.
        System.out.println("Ready for testing...");
    }

    @After
    public void cleanUp() {
        System.out.println("Done with unit test");
    }

    @Test
    public void testAdd() {

        // Exercise the domain objects' method you want to test.
        int total = sm.add(4, 5);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not adding correctly", 9, total);
    }

    @Test
    public void testDiff() {

        // Exercise the domain objects' method you want to test.
        int total = sm.diff(9, 2);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not subtracting correctly", 7, total);
    }

    /*
       Method to test the happy path of div()
     */

    @Test
    public void testDiv() {

        // Exercise the domain objects' method you want to test.
        double total = sm.div(9, 3);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not dividing correctly", 3.0, total, 0.0);
    }

    /*
       Method to test the zero path of div()
     */

    @Ignore
    @Test(expected = java.lang.ArithmeticException.class)
    public void testDivWithZeroDivisor() {

        // Exercise the domain objects' method you want to test.
        double total = sm.div(9, 0);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not handling division by zero correctly", 0.0, total, 0.0);
    }

}

/*
   NB: The assertEquals method is checking for equality among double values,
   therefore it takes a fourth value which is a delta/ the maximum delta value
   between expected and actual values.

    * The tolerance here is 0.0, suggesting the values should be exactly the same.*
 */


