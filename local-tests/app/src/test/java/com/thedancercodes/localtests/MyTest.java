package com.thedancercodes.localtests;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by TheDancerCodes on 06/03/2018.
 */

public class MyTest {

    @Test
    public void testSubtract() {
        assertEquals(4, 10-6);
    }
}
