package com.thedancercodes.androidinstrumentedtests;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by TheDancerCodes on 14/03/2018.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    /**
     * Use ActivityTestRule to create an instance of an Activity or Service (before each test method)
     * that you can exercise in test methods of that test class.
     */

    // public instance variable
    @Rule
    public MainActivityTestRule<MainActivity> mainActivityActivityTestRule = new
            MainActivityTestRule<MainActivity>(MainActivity.class);

    @Test
    public void testUI() {

        // Get a reference to the Activity created by the Rule before this method executes
        // with a call to getActivity method on the ActivityTestRule instance.
        Activity activity = mainActivityActivityTestRule.getActivity();

        // Use JUnit’s assert and the Android Activity’s findViewById method to check that the
        // Hello TextView is available on the Activity (i.e.) Is not NULL
        assertNotNull(activity.findViewById(R.id.helloTV));

        // Check that Hello TextView/ Widget is displayed on the screen.
        TextView helloView = activity.findViewById(R.id.helloTV);
        assertTrue(helloView.isShown());

        // Check that text in the TextView is the string “Hello World”
        // assertEquals("Hello World!", helloView.getText());

        // Assert using the resources reference versus hard-coding the string into your test code,
        // to check that text in the TextView is the string “Hello World”
        assertEquals(InstrumentationRegistry.getTargetContext().
                getString(R.string.hello_world), helloView.getText());

        // To prove that we are exploring the MainActivity,
        // check that when we search for a stock Android button on the Activity with findViewById,
        // it returns null.
        assertNull(activity.findViewById(android.R.id.button1));

    }

}
