package com.thedancercodes.androidinstrumentedtests;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.test.rule.ActivityTestRule;

/**
 * Created by TheDancerCodes on 15/03/2018.
 */

/**
 *
 * By creating your own ActivityTestRule class, you can explore the activity in its various states,
 * before and after the activity is being displayed on the screen.
 *
 * @param <A>
 * Allow the type of the Activity to be specified by generic parameter.
 *
 */

public class MainActivityTestRule<A extends Activity> extends ActivityTestRule<A> {


    // Code to setup the Intent as if supplied to startActivity(Intent)
    @Override
    protected Intent getActivityIntent() {
        return super.getActivityIntent();
    }

    // Code that runs before the Activity is created or launched
    @Override
    protected void beforeActivityLaunched() {
        super.beforeActivityLaunched();
    }

    // Code that runs after the Activity is launched but before the @Before or test method executes
    @Override
    protected void afterActivityLaunched() {
        super.afterActivityLaunched();
    }

    // Code that runs after the Activity is finished.
    @Override
    protected void afterActivityFinished() {
        super.afterActivityFinished();
    }

    // Launches the Activity being tested
    @Override
    public A launchActivity(@Nullable Intent startIntent) {
        return super.launchActivity(startIntent);
    }

    public MainActivityTestRule(Class<A> activityClass) {
        super(activityClass);
    }
}
