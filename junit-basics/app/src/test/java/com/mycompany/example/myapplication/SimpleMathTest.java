package com.mycompany.example.myapplication;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleMathTest {
    @Test
    public void testAdd() {

        // Instance of the Domain object/ object under test
        SimpleMath sm = new SimpleMath();

        // Exercise the domain objects' method you want to test.
        int total = sm.add(4, 5);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not adding correctly", 9, total);
    }

    @Test
    public void testDiff() {

        // Instance of the Domain object/ object under test
        SimpleMath sm = new SimpleMath(); // Instance of the Domain object/ object under test

        // Exercise the domain objects' method you want to test.
        int total = sm.diff(9, 2);

        // Confirm the results returned by the method you are testing
        assertEquals("Simple Math is not subtracting correctly", 7, total);
    }
}

/*
   NB: Assert methods are also all overloaded with a second formula method that takes a
   message string as its first parameter.

       **See above: "Simple Math is not subtracting correctly"**

       * This message string serves as the error return by the JUnit Runner whenever the Assertion check fails.
 */


