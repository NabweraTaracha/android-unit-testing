package com.mycompany.example.myapplication;

import android.app.Instrumentation;
import android.content.Context;
import android.os.RemoteException;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<MainActivity>(MainActivity.class);

    /**
     * Test that uses the UiDevice instance and exercises some of it's methods.
     *
     * @throws RemoteException - most UiDevice methods throw RemoteException.
     */
    @Test
    @Ignore
    public void testUiDevice() throws RemoteException {

        // Get the instance
        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // Check whether device screen is on;
        // If so rotate left and the pull down the notification shade
        if (device.isScreenOn()) {
            device.setOrientationLeft();
            device.openNotification();
        }
    }

    @Test
    public void testUiAutomatorAPI() throws InterruptedException, UiObjectNotFoundException {

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        // UiSelector to locate the Edit Text widget
        UiSelector editTextSelector = new UiSelector()
                .className("android.widget.EditText")
                .text("this is a test")
                .focusable(true);

        // Use UiObject to set new text in the Edit Text widget
        UiObject editTextWidget = device.findObject(editTextSelector);
        editTextWidget.setText("this is new Text");

        Thread.sleep(2000);

        // UiSelector to locate the Button
        UiSelector buttonSelector = new UiSelector()
                .className("android.widget.Button")
                .text("Click Me")
                .clickable(true);

        // Use UiObject to click the Button
        UiObject buttonWidget = device.findObject(buttonSelector);
        buttonWidget.click();

        Thread.sleep(2000);

    }



}
