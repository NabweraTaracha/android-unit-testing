package com.mycompany.example.myapplication;

import android.content.Intent;
import android.os.IBinder;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ServiceTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertNotNull;

/**
 * Created by TheDancerCodes on 29/03/2018.
 */

@RunWith(AndroidJUnit4.class)
public class MyServiceTest {

    // public instance variable
    @Rule
    public MyServiceTestRule myServiceRule = new MyServiceTestRule();


    /**
     * Test method to exemplify how to start the service using the ServiceTestRule instance variable
     * in the case of an unbound service
     *
     * @throws TimeoutException - Exception thrown when a blocking operation times out.
     */
    @Test
    public void testService() throws TimeoutException {
        myServiceRule.startService((new Intent(InstrumentationRegistry.getTargetContext(),
                MyService.class)));
    }

    /**
     * Test method to exemplify how to start the service using the ServiceTestRule instance variable
     * in the case of a bound service
     *
     * @throws TimeoutException - Exception thrown when a blocking operation times out.
     */
    @Test
    public void testBoundService() throws TimeoutException {
        IBinder binder = myServiceRule.bindService(new Intent(InstrumentationRegistry.
                getTargetContext(), MyService.class));
        MyService service = ((MyService.LocalBinder) binder).getService();

        // Do work with the Service here
        assertNotNull("Bound service is null", service);
    }

}