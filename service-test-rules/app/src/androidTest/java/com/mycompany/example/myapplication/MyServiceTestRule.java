package com.mycompany.example.myapplication;

import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.test.rule.ServiceTestRule;
import android.util.Log;

import java.util.concurrent.TimeoutException;

/**
 * Created by TheDancerCodes on 29/03/2018.
 */

public class MyServiceTestRule extends ServiceTestRule {

    @Override
    public void startService(@NonNull Intent intent) throws TimeoutException {
        Log.e("MyServiceTestRule", "start the service");
        super.startService(intent);
    }

    @Override
    protected void beforeService() {
        Log.e("MyServiceTestRule", "work before the service starts");
        super.beforeService();
    }

    @Override
    protected void afterService() {
        Log.e("MyServiceTestRule", "work after the service starts");
        super.afterService();
    }

    @Override
    public IBinder bindService(@NonNull Intent intent) throws TimeoutException {
        Log.e("MyServiceTestRule", "binding the service");
        return super.bindService(intent);
    }
}
